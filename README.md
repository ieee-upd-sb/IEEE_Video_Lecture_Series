# IEEE Video Lecture Series

## A supplementary course material for the subjects of EEE Institute | University of the Philippines Diliman | IEEE UPD Student Branch

### Dependencies

1. Git
2. Postgres
3. Node
4. Rbenv
5. Ruby
6. Rails
7. Bundle

#### ElementaryOS/Ubuntu:

Install Git, Postgres, Node:

```bash
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install libssl-dev
sudo apt-get install postgresql libpq5 libpq-dev
sudo apt-get install git nodejs
```

Note: If this is your first time on git, it's advisable to configure your credentials first:

```bash
git config --global user.name "Mark Agaton"
git config --global user.email "markallenagaton03@gmail.com"
```

Install Rbenv:

See ```https://github.com/sstephenson/rbenv``` and complete the installation instructions including the optional part:```ruby-build```.

Upon installing ```rbenv```, install Ruby 2.2.0:
```bash
rbenv install 2.2.0
rbenv local 2.2.0
```

Install Rails:

```bash
gem install rails --verbose
#--verbose command was used to see if there are errors during installation
```

Install Bundler:

```bash
gem install bundle --verbose #restart terminal after the installation
```

### Database
Create a Postgres user for the Rails app. To do this, switch into the Postgres user:
```bash
sudo -u postgres psql postgres
```

Then create a user (or a "role", as Postgres calls it):
```bash
create role "IEEE_Video_Lecture_Series" with createdb login password 'ieeeupdsb';
```

Quit the postgre REPL by typing ```\q```.

### Development Environment

1. Clone this repository to your project directory.
2. In the terminal, go to the root of the project directory.
3. Run ```bundle install```.
4. Run ```rake db:create```.
5. Run ```rake db:migrate```.
6. Run ```rails server```.
9. Using your browser, go to ```http://localhost:3000```.

### Further Notes

For a high quality and readable codebase, please make these conventions as references:

1. Ruby Style Guide ```https://github.com/bbatsov/ruby-style-guide```
2. Rails Style Guide ```https://github.com/bbatsov/rails-style-guide```

### Gem References

1. Slim ```http://slim-lang.com/```, ```https://github.com/slim-template/slim-rails```
2. Foundation ```http://foundation.zurb.com/```


### README references

UP-CSI WhatsUP README.md