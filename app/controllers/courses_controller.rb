class CoursesController < ApplicationController
  before_action :authenticate_user!
	# => add code that validates user is an admin + the links should only be available to admins

	def new
		@course = Course.new
	end

	def create
		@course = Course.new(course_params)

		if @course.save
			redirect_to @course
		else
			render 'new'
		end
		
	end

  def index
    @courses = Course.all
  end

	def edit
		@course = Course.find(params[:id])
	end

	def show
		@course = Course.find(params[:id])
	end

	def update
		@course = Course.find(params[:id])
		if @course.update(course_params)
			redirect_to @course
		else
			render 'edit'
		end
	end

	def destroy
		@course = Course.find(params[:id])
		@course.destroy
		redirect_to courses_path
	end

  def enlist
    @course = Course.find(params[:course_id])
    @student = Student.find(params[:student_id])
    render plain: 'hello'
  end

	private

		def course_params
			params.require(:course).permit(:id, :name, :dep, :course_desc)
		end
end
