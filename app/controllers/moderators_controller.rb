class ModeratorsController < ApplicationController
  before_action :authenticate_user!

  def new
    @moderator = Moderator.new
  end

  def index
    @moderators = Moderator.all
  end

  def create
    #generate random password_token
    password_token = Devise.friendly_token.first(8)
    
    @moderator = Moderator.new(moderator_params(password_token))

    if @moderator.save
      # manually send the confirmation instructions for moderators
      @moderator.send_moderator_confirmation_instructions(
        {moderator: true, 
        password_token: password_token})
      flash[:notice] = 'Moderator account added. Confirmation email sent'
      redirect_to moderators_path
    else 
      flash.now[:error] = 'Error in creating modearator account'
      render 'new'
    end
  end

  def destroy
    @moderator = Moderator.find(params[:id])
    @moderator.delete
    redirect_to moderators_path
  end

  private

  def moderator_params(password_token)
    # set the password field of the moderator account to the
    # random password_token

    moderator_params = {username: params[:moderator][:username],
                        email: params[:moderator][:email],
                        password: password_token,
                        password_confirmation: password_token}
    moderator_params
  end
end