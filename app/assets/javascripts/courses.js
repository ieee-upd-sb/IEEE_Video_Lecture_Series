$(function() {
  $('#btn-enlist').click(function() {
    var course_id = $(this).data('course-id');
    var student_id = $(this).data('student-id');

    $.ajax({
      url: '/courses/enlist',
      type: 'POST',
      data: {
        'course_id': course_id,
        'student_id': student_id
      },
      dataType: 'html',
      success: function() {
        console.log('success');
      },
      error: function(e, data) {
        console.log(e);
        console.log(data);
      }
    });
  }); 
});