class Moderator < User
  def send_moderator_confirmation_instructions(options)
    send_confirmation_instructions(options)
  end
end