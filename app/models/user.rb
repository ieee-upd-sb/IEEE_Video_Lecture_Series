class User < ActiveRecord::Base
  validates :username, presence: true, 
                       uniqueness: { case_sensitive: false }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable :trackable,
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  # Virtual attribute for authenticating by either username or email
  # This is in addition to a real persisted field like 'username'
  # Getter and Setter for 'login' virtual attribute
  attr_accessor :login

  # Single Table Inheritance
  self.inheritance_column = :role
  scope :students, -> { where(role: :Student) }
  scope :professors, -> { where(role: :Professor )}
  scope :moderators, -> { where(role: :Moderator )}
  scope :administrator, -> { find_by(role: :Administrator) }

  # Devise Override
  def send_confirmation_instructions(options={})
    unless @raw_confirmation_token
      generate_confirmation_token!
    end

    opts = pending_reconfirmation? ? { to: unconfirmed_email } : { }

    if options[:moderator]
      opts[:password_token] = options[:password_token]
      send_devise_notification(:moderator_confirmation_instructions, 
        @raw_confirmation_token, opts)
    else
      send_devise_notification(:confirmation_instructions, 
        @raw_confirmation_token, opts)
    end
  end

  def administrator?
    self.role == 'Administrator'
  end

  def moderator?
    self.role == 'Moderator'
  end

  def professor?
    self.role == 'Professor'
  end

  def student?
    self.role == 'Student'
  end

  # Devise Override
  def send_devise_notification(notification, *args)
    # for queuing devise emails as background job in ActiveJob
    devise_mailer.send(notification, self, *args).deliver_later
  end

  # Override authentication mechanism of devise
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup

    if login = conditions.delete(:login)
      where(conditions).where('username = :value OR lower(email) = lower(:value)',
        { value: login }).first
    else
      where(conditions).first
    end
  end

  def self.roles
    %w(Student Professor Moderator Administrator)
  end

  protected

  # Devise Override
  def send_on_create_confirmation_instructions
    # Prevent Devise's automatic sending of confirmation instructions on 
    # moderator account creation. Sending of confirmation instructions for new 
    # moderator accounts is defined in Moderators model file (Moderator.rb)

    unless self.moderator?
      send_confirmation_instructions
    end
  end
end