class Administrator < User
  validate :no_admin_exists

  def no_admin_exists
    errors.add(:base, 'Adminstrator account already exists.') unless Administrator.all.empty?
  end
end