class Course < ActiveRecord::Base
	validates :name, presence: true, uniqueness: true, length: { maximum: 30 }
	validates :dep, presence: true, length: { maximum: 70 }
	validates :title, presence: true, length: { maximum: 150 }
end
