class UserMailer < Devise::Mailer
  def moderator_confirmation_instructions(record, token, opts={})
    @token = token
    # pass the moderator's temporary password to the mailer view
    @password_token = opts[:password_token]
    devise_mail(record, :moderator_confirmation_instructions, opts)
  end
end