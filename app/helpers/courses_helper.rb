module CoursesHelper
  def course_options(opts={})
    if current_user.student?
      if opts[:enlist]
        enlist_tag(opts[:enlist_data])
      end
    end
  end

  def enlist_tag(enlist_data)
    tag('input', type: 'button', value: 'Enlist', id: 'btn-enlist', 
      data: {course_id: enlist_data[:course_id], student_id: enlist_data[:student_id]})
  end
end
