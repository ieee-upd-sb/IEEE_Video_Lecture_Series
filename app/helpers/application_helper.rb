module ApplicationHelper
  def flash_style(name)
    name.to_s == 'notice' ? 'success' : 'alert'
  end
end
