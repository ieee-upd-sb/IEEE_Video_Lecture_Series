module HomeHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def home_controller?
    params[:controller] == 'home'
  end

  def render_user_homepage
    if current_user.student?
      content_tag(:div, 'Student')
    elsif current_user.moderator?
      content_tag(:div, 'Moderator')
    elsif current_user.professor?
      content_tag(:div, 'Professor')
    elsif current_user.administrator?
      content_tag(:div, 'Administrator')
    end
  end
end
