Rails.application.routes.draw do
  root 'home#index'
  get 'home/index'


  devise_for :users, controllers: {
    sessions: 'users/sessions',
    passwords: 'users/passwords',
    confirmations: 'users/confirmations',
    registrations: 'users/registrations'
  }

  devise_scope :user do
    get 'sign_in', to: 'users/sessions#new'
    get 'sign_up', to: 'users/registrations#new'
    delete 'sign_out', to: 'users/sessions#destroy'
  end

  get '/courses.delete' => 'courses#destroy'

  resources :courses do
    collection do
      post 'enlist', as: :enlist
    end
  end
  resources :moderators

end
