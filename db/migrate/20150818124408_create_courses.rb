class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.string :dep
      t.string :title
      t.string :title

      t.timestamps null: false
    end
  end
end
