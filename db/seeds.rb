# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def create_main_accounts
  roles = %w(Administrator Moderator Professor Student)
  email = '@gmail.com'

  roles.each do |role|
    a = User.create(username: role.downcase,
      email: role.downcase + email,
      role: role,
      password: 'password',
      password_confirmation: 'password')
    a.confirm
  end
end

create_main_accounts
Course.create(name: "EEE 31", dep: "EEEI", title: "Introduction to Electrical and Electronics Engineering")
Course.create(name: "EEE 11", dep: "EEEI", title: "Programming Fundamentals")
Course.create(name: "CS 11", dep: "DCS", title: "Computer Programming I")
Course.create(name: "EEE 23", dep: "EEEI", title: "Electromagnetic Fields I")
Course.create(name: "CS 32", dep: "DCS", title: "Data Structures")
